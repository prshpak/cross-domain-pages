
(function (global) {
    System.config({
        paths: {
            'lib:': '/js/node_modules/',
        },
        map: {
            // angular bundles
            '@angular/core': 'lib:@angular/core/bundles/core.umd.min.js',
            '@angular/common': 'lib:@angular/common/bundles/common.umd.min.js',
            '@angular/compiler': 'lib:@angular/compiler/bundles/compiler.umd.min.js',
            '@angular/platform-browser': 'lib:@angular/platform-browser/bundles/platform-browser.umd.min.js',
            '@angular/platform-browser-dynamic': 'lib:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.min.js',
            '@angular/http': 'lib:@angular/http/bundles/http.umd.min.js',
            '@angular/router': 'lib:@angular/router/bundles/router.umd.min.js',
            '@angular/forms': 'lib:@angular/forms/bundles/forms.umd.min.js',
            '@angular/animations': 'lib:@angular/animations/bundles/animations.umd.min.js',
            '@angular/animations/browser': 'lib:@angular/animations/bundles/animations-browser.umd.min.js',
            '@angular/platform-browser/animations': 'lib:@angular/platform-browser/bundles/platform-browser-animations.umd.min.js',
            'rxjs/*': 'lib:rxjs/rxjs.min.js',
            'shared-lib': 'lib:shared-lib'
        },
        packages: {
            '/js/app': {
                defaultExtension: 'js'
            },
            'http://localhost:5000/js': {
                defaultExtension: 'js'
            },
            'shared-lib': {
                defaultExtension: 'js'
            }
        }
    });

})(this);