
import { RoutingModule } from 'shared-lib/decorators/type-decorators/routing-module.decorator';

@RoutingModule({
    routes: [
        {
            path: 'home',
            filePath: 'home-page/home-page.module',
            moduleName: 'HomePageModule'
        },
        {
            path: 'heroes',
            loadChildren: 'http://localhost:5000/js/hero-list/hero-list.module#HeroListModule'
        }
    ]
})
export class AppRotingModule {

}