import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageModule } from 'shared-lib/components/page/page.module';
import { HomePageComponent } from './home-page.component';
import { RoutingModule } from 'shared-lib/decorators/type-decorators/routing-module.decorator';
import { RootRouterModule } from '../modules/root-router.module';

@RoutingModule({
    ngModule: {
        imports: [
            CommonModule, PageModule, RootRouterModule
        ],
        declarations: [
            HomePageComponent
        ]
    },
    routes: [
        {
            path: '',
            component: HomePageComponent,
            children: [
                {
                    path: 'weather',
                    filePath: 'weather/weather.module',
                    moduleName: 'WeatherModule'
                },
                {
                    path: 'heroes',
                    loadChildren: 'http://localhost:5000/js/hero-list/hero-list.module#HeroListModule'
                }
            ]
        }
    ]
})
export class HomePageModule {
}