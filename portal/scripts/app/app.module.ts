import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MainNavModule } from './components/main-nav/main-nav.module';
import { AppRotingModule } from './app-routing.module';
import { RootRouterModule } from './modules/root-router.module';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot([]),
        MainNavModule,
        RootRouterModule,
        AppRotingModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}