import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageModule } from 'shared-lib/components/page/page.module';
import { RoutingModule } from 'shared-lib/decorators/type-decorators/routing-module.decorator';

import { WeatherComponent } from './weather.component';
import { RootRouterModule } from '../modules/root-router.module';

@RoutingModule({
    ngModule: {
        imports: [
            CommonModule, PageModule, RootRouterModule
        ],
        declarations: [
            WeatherComponent
        ]
    },
    routes: [
        {
            path: '',
            component: WeatherComponent
        }
    ]
})
export class WeatherModule {
}