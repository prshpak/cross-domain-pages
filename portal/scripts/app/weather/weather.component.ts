import { Component, OnInit } from '@angular/core';
import { AppWebService } from 'shared-lib/web-services/app.web-service';

@Component({
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
    forecasts: WeatherForecast[];

    constructor(private readonly AppWebService: AppWebService) {

    }

    ngOnInit() {
        this.AppWebService
            .get<WeatherForecast[]>('WeatherForecast')
            .subscribe(forecasts => this.forecasts = forecasts);
    }
}

type WeatherForecast = {
    dateFormatted: string;
    temperatureC: number;
    temperatureF: number;
    summary: string;
}
