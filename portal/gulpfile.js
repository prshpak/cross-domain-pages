var gulp = require('gulp');
var rimraf = require('rimraf');
var gulpTypescript = require('gulp-typescript');
var inlineNg2Template = require('gulp-inline-ng2-template');
var runSequence = require('run-sequence');
var gulpSass = require('gulp-sass');
var nodeSass = require('node-sass');
var systemjsBuilder = require('systemjs-builder');

var jsLocation = './wwwroot/js/';

var paths = {
    libsSource: './node_modules/',
    jsLocation: './wwwroot/js/',
    appSource: './scripts/',
    stylesSource: './styles/',
    stylesTarget: './wwwroot/styles/'
};

var libs = [
    '@angular/**/bundles/*.min.js',
    'shared-lib/**/*.js',
    'core-js/client/shim.min.js',
    'systemjs/dist/system.src.js',
    'zone.js/dist/zone.js',
    'zone.js/dist/long-stack-trace-zone.js'
];

var tasks = {
    build: 'build',
    buildStyles: 'build:styles',
    buildTs: 'build:ts',
    buildJs: 'build:js',
    buildModules: 'build:modules',
    buildModulesCopy: 'build:modules:copy',
    buildModulesRxjs: 'build:modules:rxjs',
    clean: 'clean',
    cleanJs: 'clean:js',
    watchAll: 'watch:all',
    watchStyles: 'watch:styles',
    watchTs: 'watch:ts',
    watchJs: 'watch:js'
}

gulp.task(tasks.build, [tasks.buildTs, tasks.buildJs, tasks.buildModules, tasks.buildStyles]);

gulp.task(tasks.buildTs, function () {
    var tsProject = gulpTypescript.createProject('./tsconfig.json', { typescript: require('typescript') });

    return gulp
        .src(paths.appSource + '**/*.ts')
        .pipe(inlineNg2Template(
            {
                useRelativePaths: true,
                styleProcessor: function (path, ext, file, cb) {
                    nodeSass.render(
                        {
                            file: path
                        },
                        function (error, result) {
                            if (error) {
                                cb(error);
                            }

                            cb(null, result.css);
                        });
                }
            }
        ))
        .pipe(tsProject())
        .pipe(gulp.dest(paths.jsLocation));
});

gulp.task(tasks.buildJs, function () {
    return gulp
        .src(`${paths.appSource}**/*.js`)
        .pipe(gulp.dest(paths.jsLocation));
});

gulp.task(tasks.buildModules, [tasks.buildModulesCopy, tasks.buildModulesRxjs]);

gulp.task(tasks.buildModulesCopy, function () {
    return gulp
        .src(libs.map(lib => `${paths.libsSource}${lib}`), { base: '.' })
        .pipe(gulp.dest(paths.jsLocation));
});

gulp.task(tasks.buildModulesRxjs, function () {
    var builder = new systemjsBuilder('./');
    builder.config({
        paths: { 'rxjs/*': `${paths.libsSource}rxjs/*.js` },
        map: { 'rxjs': `${paths.libsSource}rxjs` },
        packages: { 'rxjs': { main: 'Rx.js', defaultExtension: 'js' } }
    });

    builder.bundle('rxjs', `${paths.jsLocation}node_modules/rxjs/rxjs.min.js`, {
        minify: true,
        mangle: true
    });
});

gulp.task(tasks.buildStyles, function () {
    return gulp.src(`${paths.stylesSource}*.scss`)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulp.dest(paths.stylesTarget));
});

/*-- watch tasks --*/

gulp.task(tasks.watchAll, [tasks.watchStyles, tasks.watchJs, tasks.watchTs]);

gulp.task(tasks.watchStyles, function () {
    return gulp.watch(`${paths.stylesSource}**/*.scss`, [tasks.buildStyles]);
});

gulp.task(tasks.watchJs, function () {
    return gulp.watch(`${paths.appSource}**/*.js`, [tasks.buildJs]);
});

gulp.task(tasks.watchTs, function () {
    return gulp.watch([`${paths.appSource}**/*.ts`, `${paths.appSource}**/*.html`, `${paths.appSource}**/*.scss`], [tasks.buildTs]);
});

/*-- Clean tasks --*/
gulp.task(tasks.clean, [tasks.cleanJs]);

gulp.task(tasks.cleanJs, function (callback) {
    var toDelete = [paths.jsLocation];
    var i = 0;
    var cb = function () {
        if (++i == toDelete.length)
            callback();
    };
    toDelete.forEach(function (path) {
        rimraf(path, {}, cb);
    });
});
