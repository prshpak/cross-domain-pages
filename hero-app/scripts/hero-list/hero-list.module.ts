import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageModule } from 'shared-lib/components/page/page.module';
import { RoutingModule } from 'shared-lib/decorators/type-decorators/routing-module.decorator';

import { RootRouterModule } from '../modules/root-router.module';
import { HeroService } from '../services/hero.service';
import { HeroListComponent } from './hero-list.component';

@RoutingModule({
    ngModule: {
        imports: [
            CommonModule, PageModule, RootRouterModule
        ],
        declarations: [
            HeroListComponent
        ],
        providers: [HeroService]
    },
    routes: [
        {
            path: '',
            component: HeroListComponent,
            children: [
                {
                    path: ':id',
                    filePath: 'hero-editor/hero-editor.module',
                    moduleName: 'HeroEditorModule'
                }
            ]
        }
    ]
})
export class HeroListModule {
}