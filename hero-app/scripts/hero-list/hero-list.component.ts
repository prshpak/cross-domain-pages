import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppWebService } from 'shared-lib/web-services/app.web-service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';

@Component({
    templateUrl: './hero-list.component.html',
    styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit, OnDestroy {
    heroes: Observable<Hero[]>;
    private destroyed = new Subject<void>();

    constructor(private readonly heroService: HeroService) {

    }

    ngOnInit() {
        this.heroService
            .heroes
            .takeUntil(this.destroyed)
            .subscribe(e => this.heroes = e);
    }

    ngOnDestroy() {
        this.destroyed.next();
    }
}