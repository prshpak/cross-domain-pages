﻿
let isExternalRegex = new RegExp('^http[s]?:\/\/*');
let fileParts = __filename.split('/');
let currentDomain = isExternalRegex.test(__filename) ? fileParts.slice(0, 3).join('/') : ''; // http://domain.name contains 3 parts (one empty)  
let scriptsFolder = fileParts.slice(0, fileParts.length - 2).join('/'); // in oder do get to the scripts root we need to scipt last 2 parts

export class UrlUtility {
    getApiUrl(relativeUrl: string) {
        return `${currentDomain}/api/${relativeUrl}`;
    }

    getFilePath(path: string) {
        return `${scriptsFolder}/${path}`;
    }
}

