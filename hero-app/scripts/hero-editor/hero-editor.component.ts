import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { PageComponent } from 'shared-lib/components/page/page.component';

import { HeroService } from '../services/hero.service';
import { Hero } from '../models/hero';

@Component({
    templateUrl: './hero-editor.component.html',
    styleUrls: ['./hero-editor.component.scss']
})
export class HeroEditorComponent implements OnInit, OnDestroy {
    hero: Hero;
    private destroyed = new Subject<void>();
    @ViewChild(PageComponent) page: PageComponent;

    constructor(
        private readonly activedRoute: ActivatedRoute,
        private readonly heroService: HeroService
    ) {

    }

    ngOnInit() {
        this.activedRoute
            .params
            .takeUntil(this.destroyed)
            .subscribe(params => this.heroService.setCurrent(+[params['id']]));

        this.heroService
            .currentHero
            .takeUntil(this.destroyed)
            .subscribe(source => source
                .takeUntil(this.destroyed)
                .subscribe(e => this.hero = e)
            );
    }

    ngOnDestroy() {
        this.destroyed.next();
        this.heroService.setCurrent(null);
    }

    save() {
        this.hero
            ? this.heroService
                .save(this.hero)
                .subscribe(() => this.close())
            : this.close();
    }

    close() {
        this.page && this.page.closePage();
    }
}
