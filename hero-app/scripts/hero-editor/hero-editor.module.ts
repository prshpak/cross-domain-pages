import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PageModule } from 'shared-lib/components/page/page.module';
import { HeroEditorComponent } from './hero-editor.component';
import { RoutingModule } from 'shared-lib/decorators/type-decorators/routing-module.decorator';

@RoutingModule({
    ngModule: {
        imports: [
            CommonModule, PageModule, FormsModule
        ],
        declarations: [
            HeroEditorComponent
        ]
    },
    routes: [
        {
            path: '',
            component: HeroEditorComponent,
            children: [
                {
                    path: 'weather',
                    loadChildren: 'http://localhost:5050/js/app/weather/weather.module#WeatherModule'
                }
            ]
        }
    ]
})
export class HeroEditorModule {
}