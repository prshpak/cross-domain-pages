import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';

import { AppWebService } from 'shared-lib/web-services/app.web-service';
import { Hero } from '../models/hero';

@Injectable()
export class HeroService {
    get heroes(): Observable<Observable<Hero[]>> {
        return this._heroes;
    }
    get currentHero(): Observable<Observable<Hero>> {
        return this._heroId.map(id => id ? this.appWebService.get<Hero>(`Hero/${id}`) : Observable.of({ id: 0, name: null }));
    }
    _heroes = new ReplaySubject<Observable<Hero[]>>(1);
    _heroId = new ReplaySubject<number>(1)

    constructor(private readonly appWebService: AppWebService) {
        this.resetHeroes();
    }

    setCurrent(id: number) {
        this._heroId.next(id);
    }

    save(hero: Hero) {
        return hero.id ? this.updateHero(hero.id, hero.name) : this.addHero(hero.name);
    }

    addHero(name: string) {
        return this.appWebService.post('Hero', name).do(() => this.resetHeroes());
    }

    updateHero(id: number, name: string) {
        return this.appWebService.put(`Hero/${id}`, name).do(() => this.resetHeroes());
    }

    deleteHero(id: number) {
        return this.appWebService.delete(`Hero/${id}`).do(() => this.resetHeroes());
    }

    private resetHeroes() {
        this._heroes.next(this.appWebService.get<Hero[]>('Hero'));
    }
}