﻿import { NgModule } from '@angular/core';

import { UrlUtility } from '../helpers/url.utility';
import { UrlUtility as SharedUrlUtility } from 'shared-lib/helpers/url.utility';
import { AppWebService } from 'shared-lib/web-services/app.web-service';

@NgModule({
    providers: [{ provide: SharedUrlUtility, useClass: UrlUtility }, AppWebService]
})
export class RootRouterModule {
}
