using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace heroApp.Controllers
{
    [Route("api/[controller]")]
    public class HeroController : Controller
    {
        [HttpGet]
        public IEnumerable<Hero> List()
        {
            return _context;
        }

        [HttpGet("{id}")]
        public Hero Get(int id)
        {
            return _context.First(h => h.Id == id);
        }

        [HttpPost]
        public void Create([FromBody] string name)
        {
            var newId = _context.Count > 0 ? _context.Max(h => h.Id) + 1 : 1;
            _context.Add(new Hero { Id = newId, Name = name });
        }

        [HttpPut("{id}")]
        public void Update(int id, [FromBody] string name)
        {
            _context.First(h => h.Id == id).Name = name;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _context.RemoveAll(h => h.Id == id);
        }

        private static List<Hero> _context = new List<Hero>
        {
            new Hero { Id = 1, Name = "Mr. Nice"},
            new Hero { Id = 2, Name = "Narco"},
            new Hero { Id = 3, Name = "Bombasto"},
            new Hero { Id = 4, Name = "Celeritas"},
            new Hero { Id = 5, Name = "Magneta"},
            new Hero { Id = 6, Name = "RubberMan"},
            new Hero { Id = 7, Name = "Dynama"},
            new Hero { Id = 8, Name = "Dr IQ"},
            new Hero { Id = 9, Name = "Magma"},
            new Hero { Id = 10, Name = "Tornado"}
        };

        public class Hero
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
