var gulp = require('gulp');
var rimraf = require('rimraf');
var gulpTypescript = require('gulp-typescript');
var inlineNg2Template = require('gulp-inline-ng2-template');
var runSequence = require('run-sequence');
var gulpSass = require('gulp-sass');
var nodeSass = require('node-sass');
var systemjsBuilder = require('systemjs-builder');

var jsLocation = './wwwroot/js/';

var paths = {
    jsLocation: './wwwroot/js/',
    appSource: './scripts/'
};

var tasks = {
    build: 'build',
    clean: 'clean',
    watchTs: 'watch:ts'
}


gulp.task(tasks.build, function () {
    var tsProject = gulpTypescript.createProject('./tsconfig.json', { typescript: require('typescript') });

    return gulp
        .src(paths.appSource + '**/*.ts')
        .pipe(inlineNg2Template(
            {
                useRelativePaths: true,
                styleProcessor: function (path, ext, file, cb) {
                    nodeSass.render(
                        {
                            file: path
                        },
                        function (error, result) {
                            if (error) {
                                cb(error);
                            }

                            cb(null, result.css);
                        });
                }
            }
        ))
        .pipe(tsProject())
        .pipe(gulp.dest(paths.jsLocation));
});

/*-- watch tasks --*/

gulp.task(tasks.watchTs, function () {
    return gulp.watch([`${paths.appSource}**/*.ts`, `${paths.appSource}**/*.html`, `${paths.appSource}**/*.scss`], [tasks.build]);
});

/*-- Clean tasks --*/

gulp.task(tasks.clean, function (callback) {
    var toDelete = [paths.jsLocation];
    var i = 0;
    var cb = function () {
        if (++i == toDelete.length)
            callback();
    };
    toDelete.forEach(function (path) {
        rimraf(path, {}, cb);
    });
});
