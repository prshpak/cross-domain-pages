import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { UrlUtility } from '../helpers/url.utility';

@Injectable()
export class AppWebService {

    constructor(
        private readonly http: Http,
        private readonly urlUtility: UrlUtility
    ) {
    }

    get<T>(url: string, process: (response: any) => T = e => e): Observable<T> {
        return this.http.get(this.urlUtility.getApiUrl(url)).map(response => process(response.text() && response.json()));
    }

    post<T>(url: string, data: any, process: (response: any) => T = e => e): Observable<T> {
		let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.urlUtility.getApiUrl(url), JSON.stringify(data), { headers: headers }).map(response => process(response.text() && response.json()));
    }

    put<T>(url: string, data: any, process: (response: any) => T = e => e): Observable<T> {
		let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put(this.urlUtility.getApiUrl(url), JSON.stringify(data), { headers: headers }).map(response => process(response.text() && response.json()));
    }

    delete<T>(url: string, process: (response: any) => T = e => e): Observable<T> {
        return this.http.delete(this.urlUtility.getApiUrl(url)).map(response => process(response.text() && response.json()));
    }
}