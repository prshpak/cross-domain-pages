

import { ActivatedRoute } from '@angular/router';

export class RouterUtility {
    static getParentRoute(route: ActivatedRoute) {
        let parent = route.parent;
        while (parent && !parent.component && parent.parent) {
            parent = parent.parent;
        }

        return parent;
    }
}