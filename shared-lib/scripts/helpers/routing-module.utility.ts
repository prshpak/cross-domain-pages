﻿import { NgModule, Type } from '@angular/core';
import { RouterModule, ROUTES } from '@angular/router';

import { UrlUtility } from './url.utility';
import { AppRoute } from '../models/app-route';
import { AppRouteAsyncModel } from '../models/app-route-async.model';

export class RoutingModuleUtility {
    static addRoutes(module: NgModule, routes: AppRoute[]): NgModule {
        return RoutingModuleUtility.mergeModules(module || {}, RoutingModuleUtility.getRouteModule(routes));
    }

    static getRouteModule(routes: AppRoute[]): NgModule {
        let components: Type<any>[] = [];
        RoutingModuleUtility.traverseRoutes(routes, (r) => !RoutingModuleUtility.isAsync(r) && r.component && components.push(r.component));

        return {
            imports: [
                RouterModule.forChild([])
            ],
            providers: [
                {
                    provide: ROUTES,
                    useFactory: (urlUtility: UrlUtility) => {
                        return RoutingModuleUtility.mapRoutes(routes,
                            (r) => RoutingModuleUtility.isAsync(r)
                                ? { ...r, loadChildren: `${urlUtility.getFilePath(r.filePath)}#${r.moduleName}` }
                                : r
                        );
                    },
                    deps: [UrlUtility],
                    multi: true
                }
            ],
            entryComponents: components
        };
    }

    static mergeModules(...modules: NgModule[]): NgModule {
        return modules && modules.length
            ? {
                providers: RoutingModuleUtility.mergeItems(modules, (e) => e.providers),
                declarations: RoutingModuleUtility.mergeItems(modules, (e) => e.declarations),
                imports: RoutingModuleUtility.mergeItems(modules, (e) => e.imports),
                exports: RoutingModuleUtility.mergeItems(modules, (e) => e.exports),
                entryComponents: RoutingModuleUtility.mergeItems(modules, (e) => e.entryComponents),
                bootstrap: RoutingModuleUtility.mergeItems(modules, (e) => e.bootstrap),
                schemas: RoutingModuleUtility.mergeItems(modules, (e) => e.schemas)
            }
            : {};
    }

    private static mergeItems<T>(modules: NgModule[], func: (m: NgModule) => T[]): T[] {
        return modules
            ? [].concat(...modules.map(e => func(e) || [])).filter((item, index, items) => items.indexOf(item) === index)
            : [];
    }

    private static traverseRoutes(routes: AppRoute[], action: (r: AppRoute) => void): void {
        if (!routes) return;

        routes.forEach((r) => {
            action(r);
            !RoutingModuleUtility.isAsync(r) && RoutingModuleUtility.traverseRoutes(r.children, action);
        });
    }

    private static mapRoutes<T>(routes: AppRoute[], map: (r: AppRoute) => AppRoute): AppRoute[] {
        if (!routes) return routes;

        return routes.map(r => RoutingModuleUtility.isAsync(r)
            ? map(r)
            : { ...map(r), children: RoutingModuleUtility.mapRoutes(r.children, map) }
        );
    }

    private static isAsync(obj: any): obj is AppRouteAsyncModel {
        return obj.moduleName !== undefined && obj.filePath !== undefined;
    }
}
