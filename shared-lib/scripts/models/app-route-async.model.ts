﻿import { Data, ResolveData, RunGuardsAndResolvers } from '@angular/router';
import { UrlMatcher } from '@angular/router/src/config'; //TODO is included '@angular/router' in exports in latest version of angular 

export interface AppRouteAsyncModel {
    moduleName: string;
    filePath: string;
    /* From Route */
    path?: string;
    pathMatch?: string;
    matcher?: UrlMatcher;
    outlet?: string;
    canActivate?: any[];
    canActivateChild?: any[];
    canDeactivate?: any[];
    canLoad?: any[];
    data?: Data;
    resolve?: ResolveData;
    runGuardsAndResolvers?: RunGuardsAndResolvers;
}
