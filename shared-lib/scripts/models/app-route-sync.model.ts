﻿import { Route } from '@angular/router';

import { AppRoute } from './app-route';

export interface AppRouteSyncModel extends Route {
    children?: AppRoute[];
}
