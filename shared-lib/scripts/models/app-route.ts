﻿
import { AppRouteAsyncModel } from './app-route-async.model';
import { AppRouteSyncModel } from './app-route-sync.model';

export type AppRoute = AppRouteAsyncModel | AppRouteSyncModel;





