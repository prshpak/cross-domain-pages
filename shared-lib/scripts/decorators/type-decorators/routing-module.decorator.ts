﻿import { NgModule, TypeDecorator } from '@angular/core';

import { RoutingModuleUtility } from '../../helpers/routing-module.utility';
import { AppRoute } from '../../models/app-route';


export const RoutingModule = (config: { ngModule?: NgModule, routes: AppRoute[] }): TypeDecorator => {
    return NgModule(RoutingModuleUtility.addRoutes(config.ngModule, config.routes));
}