import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterUtility } from '../../helpers/router.utility';

@Component({
    selector: 'page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss']
})
export class PageComponent {
    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly router: Router
    ) {

    }

    closePage() {
        this.router.navigate(['./'], { relativeTo: RouterUtility.getParentRoute(this.activatedRoute) });
    }
}
