var gulp = require('gulp');
var rimraf = require('rimraf');
var gulpTypescript = require('gulp-typescript');
var inlineNg2Template = require('gulp-inline-ng2-template');
var nodeSass = require('node-sass');
var merge = require('merge2');

var paths = {
    appSource: './scripts/',
    libName: 'shared-lib',
    targets: ['../portal/node_modules/', '../hero-app/node_modules/']
};

var tasks = {
    build: 'build'
}

gulp.task(tasks.build, function () {
    var tsProject = gulpTypescript.createProject('./tsconfig.json', { typescript: require('typescript') });

    var tsResult = gulp
        .src(paths.appSource + '**/*.ts')
        .pipe(inlineNg2Template(
            {
                useRelativePaths: true,
                declaration: true,
                styleProcessor: function (path, ext, file, cb) {
                    nodeSass.render(
                        {
                            file: path
                        },
                        function (error, result) {
                            if (error) {
                                cb(error);
                            }

                            cb(null, result.css);
                        });
                }
            }
        ))
        .pipe(tsProject());


    var dtsResult = tsResult.dts;
    var jsResult = tsResult.js;

    paths.targets.forEach((target) => {
        var dest = `${target}${paths.libName}`;
        dtsResult.pipe(gulp.dest(dest));
        jsResult.pipe(gulp.dest(dest));
    });

    return merge([
        dtsResult,
        jsResult
    ]);
});
